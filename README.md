Case Study 3
===============

KURULUM
------------

``` bash
$ composer install
```
## Opsiyonel

Private ve Public key dosyaları değiştirilmek istenirse

``` bash
$ openssl genpkey -out config/jwt/private.pem -aes256 -algorithm rsa -pkeyopt rsa_keygen_bits:4096
$ openssl pkey -in config/jwt/private.pem -out config/jwt/public.pem -pubout
```

``` yaml
JWT_PASSPHRASE= Şifre
```

Database
------------

``` bash
$ php bin/console doctrine:database:create
$ php bin/console doctrine:schema:update --force
```

Kullanıcı
------------

Aşağıdaki komut yazıldıktan sonra kullanıcı ve şifre sorma adımları gelecektir.

``` bash
$ php bin/console app:add-user
```

TEST
------------

- Postman Colelction dan Auth -> Login kısmında json alanlara kullanıcı adı ve şifresi yazılır.
- Sonuç olarak dönen token enviroment'ta token değişkenine değer olarak girilir (koleksiyonda login fonksiyonunun dönüşünde tokenı enviromenta otomatik ekleyen kod var. Çalışmaması durumunda elle ekleyebilirsiniz.)
- Giriş yapmış kullanıcıya Address > Add ile bir adres tanımlanır.
- Ürün eklemek için bir fonksiyon bulunmadığında veritabanına elle bir kaç ürün girebilirsiniz.
- Product > List ile eklenen ürünler listelenir
- Order > Add Product to Basket ile ürün id si kullnılarak sepete ürün atılır
- Order > Update Quantity or Product ile eklenmiş bir ürün yada sayısı değiştirilebilir
- Order > Create ile sipariş verebilirsiniz
- Order > Remove Product from Basket ile sepetteki (sipariş statusu s olmadığı sürece) bir ürünü kaldırabilirsiniz