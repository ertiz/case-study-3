<?php


namespace App\Controller\Api;


use App\Repository\ProductRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
/**
 * @Route("/product", name="product_")
 */
class Product extends BaseApiController
{
    /**
     * @Route("s", name="list", methods={"GET"})
     * ürün listemeleri yapılır
     */
    public function list(Request $request, ProductRepository $productRepository)
    {

        $products = $productRepository->findAll();
        return $this->json($products,200);
    }
}