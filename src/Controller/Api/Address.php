<?php


namespace App\Controller\Api;


use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/address", name="address_")
 */
class Address extends BaseApiController
{
    /**
     * @Route("/add", name="add", methods={"POST"})
     * Yeni adres eklemesi yapılır
     */
    public function add(Request $request, EntityManagerInterface $entityManager)
    {

        $data =  json_decode($request->getContent());

        $title = $data->title;
        $address = $data->address;

        $addressEntity = new \App\Entity\Address();
        $addressEntity->setTitle($title);
        $addressEntity->setAddress($address);
        $addressEntity->setUser($this->getUser());
        $entityManager->persist($addressEntity);
        $entityManager->flush();
        return $this->json($addressEntity,200);
    }
}