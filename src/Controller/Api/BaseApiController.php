<?php


namespace App\Controller\Api;


use JMS\Serializer\SerializerBuilder;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;

class BaseApiController extends AbstractController
{

    public function json($data, int $status = 200, array $headers = [], array $context = []): JsonResponse
    {
        $serializer = SerializerBuilder::create()->build();
        $jsonContent = $serializer->serialize($data, 'json');
        $response =  new JsonResponse([], $status, $headers);

        return $response->setContent($jsonContent);
    }
}