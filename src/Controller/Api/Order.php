<?php


namespace App\Controller\Api;


use App\Entity\OrderDetail;
use App\Repository\AddressRepository;
use App\Repository\OrderDetailRepository;
use App\Repository\OrderRepository;
use App\Repository\ProductRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
/**
 * @Route("/order", name="order_")
 */
class Order extends BaseApiController
{
    /**
     * @Route("s", name="list", methods={"GET"})
     * Gönderilmiş siparişleri listeler
     */
    public function list(Request $request, OrderRepository $orderRepository)
    {
        $orders = $orderRepository->findOrders($this->getUser());
        return $this->json($orders ?? [],200);
    }

    /**
     * @Route("/basket", name="basket", methods={"GET"})
     * Sepetteki ürünleri listeler
     */
    public function basket(Request $request, OrderRepository $orderRepository)
    {
        $basket = $orderRepository->findBasket($this->getUser());
        return $this->json($basket ?? [],200);
    }

    /**
     * @Route("/add", name="add", methods={"POST"})
     * Sepete Ürün ekler
     */
    public function add(Request $request, OrderRepository $orderRepository, ProductRepository $productRepository)
    {
        $data = json_decode($request->getContent());
        $product = $productRepository->find($data->product);

        // Ürün olup olmadığının kontrolü yapılır
        if(!$product)
        {
            return $this->json(['code'=>404,'message'=>'Ürün Bulunamadı']);
        }
        $basket = $orderRepository->findBasket($this->getUser()) ?? new \App\Entity\Order();

        // Eğer hazırda sepet yoksa o statusu ile bir sepet oluşturulur
        if($basket->getId() === null)
        {
            $basket->setOrderCode(date('YmdHist'));
            $basket->setUser($this->getUser());
        }

        $basket->addProduct($product,$data->quantity);

        $this->getDoctrine()->getManager()->persist($basket);
        $this->getDoctrine()->getManager()->flush();
        return $this->json($basket,200);
    }

    /**
     * @Route("/update/{orderDetailId}", name="update", methods={"PATCH"})
     * Sepetteki bir ürünü yada addetini değiştirir
     */
    public function update(Request $request, OrderDetailRepository $orderDetailRepository, ProductRepository $productRepository, $orderDetailId)
    {
        $data = json_decode($request->getContent());

        $orderDetail = $orderDetailRepository->find($orderDetailId);

        // ürün detayı mevcutmu kontroü yapılır
        if(!$orderDetail)
        {
            return $this->json(['code'=>404,'message'=>'Ürün Detayı Bulunamadı']);
        }

        //Eğer sipariş gönderilmiş ise düzenlemeye izin verilmez
        if($orderDetail->getOrder()->getStatus()=='s')
        {
            return $this->json(['code'=>401,'message'=>'Bu Sipariş üzerinde değişiklik yapamazsınız']);
        }

        //ürün id si null gönderilmemiş ise ürün değişikliği yapılır
        if(!is_null($data->product))
        {
            $product = $productRepository->find($data->product);
            // ürün kontrolü yapılır yok ise hata döner
            if(!$product)
            {
                return $this->json(['code'=>404,'message'=>'Ürün Bulunamadı']);
            }
            $orderDetail->setProduct($product);
        }
        $orderDetail->setQuantity($data->quantity);

        $this->getDoctrine()->getManager()->persist($orderDetail);
        $this->getDoctrine()->getManager()->flush();

        return $this->json($orderDetail,200);
    }

    /**
     * @Route("/delete/{orderDetailId}", name="delete", methods={"DELETE"})
     */
    public function delete(Request $request, OrderDetailRepository $orderDetailRepository, ProductRepository $productRepository, $orderDetailId)
    {
        $data = json_decode($request->getContent());

        $orderDetail = $orderDetailRepository->find($orderDetailId);
        // ürün detayı mevcutmu kontroü yapılır
        if(!$orderDetail)
        {
            return $this->json(['code'=>404,'message'=>'Ürün Detayı Bulunamadı']);
        }

        //Eğer sipariş gönderilmiş ise düzenlemeye izin verilmez
        if($orderDetail->getOrder()->getStatus()=='s')
        {
            return $this->json(['code'=>401,'message'=>'Bu Sipariş üzerinde değişiklik yapamazsınız']);
        }
        $basket = $orderDetail->getOrder();

        $this->getDoctrine()->getManager()->remove($orderDetail);
        $this->getDoctrine()->getManager()->flush();

        return $this->json($basket,200);
    }

    /**
     * @Route("/create", name="create", methods={"POST"})
     */
    public function create(Request $request, OrderRepository $orderRepository, AddressRepository $addressRepository)
    {
        $data = json_decode($request->getContent());

        $basket = $orderRepository->findBasket($this->getUser());
        //Sepet gönderilmek istendiğinde sepette ürün yoksa hata verir
        if(!$basket || $basket->getOrderDetail()->count() === 0)
        {
            return $this->json(['code'=>404,'message'=>'Sepetinizde Hiç Ürün Yok']);
        }

        $address = $addressRepository->findAddressByUser($data->address,$this->getUser());

        // Kullanıcıya ait adres bulunamazsa hata verir
        if(!$address)
        {
            return $this->json(['code'=>404,'message'=>'Böyle bir adres yok']);
        }

        $basket->setStatus('p');
        $basket->setAddress($address);
        $basket->setOrderAt(new \DateTime('now'));

        $this->getDoctrine()->getManager()->persist($basket);
        $this->getDoctrine()->getManager()->flush();

        return $this->json($basket,200);
    }
}