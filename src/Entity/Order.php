<?php

namespace App\Entity;

use App\Repository\OrderRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
/**
 * @ORM\Entity(repositoryClass=OrderRepository::class)
 * @ORM\Table(name="`order`")
 */
class Order
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $orderCode;


    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @var User
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="Address")
     * @ORM\JoinColumn(name="address_id", referencedColumnName="id", nullable=true)
     * @var Address
     */
    private $address;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $orderAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $shippingAt;

    /**
     * @ORM\Column(type="string", length=1)
     * o - (opened) sepet durumundaki sipariş listesi
     * w - (waiting) sipariş edilmiş ve onay bekleyen sipariş listesi
     * p - (prepare) sipariş hazırlanıyor
     * s - (shipping) kargoda
     */
    private $status='o';

    /**
     * @ORM\OneToMany(targetEntity="OrderDetail", mappedBy="order")
     * @var ArrayCollection
     * @JMS\Type("ArrayCollection<App\Entity\OrderDetail>")
     * @JMS\MaxDepth(1)
     */
    private $orderDetail;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOrderCode(): ?string
    {
        return $this->orderCode;
    }

    public function setOrderCode(string $orderCode): self
    {
        $this->orderCode = $orderCode;

        return $this;
    }

    public function getAddress()
    {
        return $this->address;
    }

    public function setAddress($address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getOrderAt(): ?\DateTimeInterface
    {
        return $this->orderAt;
    }

    public function setOrderAt(\DateTimeInterface $orderAt): self
    {
        $this->orderAt = $orderAt;

        return $this;
    }

    public function getShippingAt(): ?\DateTimeInterface
    {
        return $this->shippingAt;
    }

    public function setShippingAt(\DateTimeInterface $shippingAt): self
    {
        $this->shippingAt = $shippingAt;

        return $this;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getOrderDetail()
    {
        return $this->orderDetail;
    }

    /**
     * @param mixed $orderDetail
     */
    public function setOrderDetail($orderDetail)
    {
        $this->orderDetail = $orderDetail;
    }

    public function addProduct(Product $product, $quantity)
    {
        $filter = $this->orderDetail->filter(function($item) use ($product,$quantity){
            if($item->getProduct() == $product)
            {
                $item->setQuantity($item->getQuantity()+$quantity);
                return $item;
            }
        });

        if($filter->count()===0)
        {
            $orderDetail = new OrderDetail();
            $orderDetail->setProduct($product);
            $orderDetail->setPrice($product->getPrice());
            $orderDetail->setQuantity(intval($quantity) ?? 1);
            $this->addOrderDetail($orderDetail);
        }
        return $this;
    }
    /**
     * @param OrderDetail $orderDetail
     */
    public function addOrderDetail(OrderDetail $orderDetail)
    {
        if (!$this->orderDetail->contains($orderDetail)) {
            return;
        }
        $this->orderDetail->addElement($orderDetail);
        $orderDetail->setOrder($this);
    }
}
