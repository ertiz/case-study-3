<?php

namespace App\Repository;

use App\Entity\Order;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Order|null find($id, $lockMode = null, $lockVersion = null)
 * @method Order|null findOneBy(array $criteria, array $orderBy = null)
 * @method Order[]    findAll()
 * @method Order[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrderRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Order::class);
    }

    public function findBasket(User $user, $status='o')
    {
        $qb = $this->createQueryBuilder('o');
        $qb->leftJoin('o.user','user');
        $qb->where('user = :user')->setParameter('user',$user);
        $qb->andWhere('o.status = :status')->setParameter('status',$status);
        return $qb->setMaxResults(1)->getQuery()->getOneOrNullResult();
    }

    public function findOrders(User $user)
    {
        $qb = $this->createQueryBuilder('o');
        $qb->leftJoin('o.user','user');
        $qb->where('user = :user')->setParameter('user',$user);
        $qb->andWhere('o.status <> :status')->setParameter('status','o');
        $qb->addOrderBy('o.orderAt','DESC');
        return $qb->setMaxResults(1)->getQuery()->getOneOrNullResult();
    }
}
