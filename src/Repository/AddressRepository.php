<?php

namespace App\Repository;

use App\Entity\Address;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Address|null find($id, $lockMode = null, $lockVersion = null)
 * @method Address|null findOneBy(array $criteria, array $orderBy = null)
 * @method Address[]    findAll()
 * @method Address[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AddressRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Address::class);
    }

    public function findAddressByUser($id, User $user)
    {
        $qb = $this->createQueryBuilder('a');
        $qb->leftJoin('a.user','user');
        $qb->where('user = :user')->setParameter('user',$user);
        $qb->andWhere('a.id = :id')->setParameter('id',$id);
        return $qb->setMaxResults(1)->getQuery()->getOneOrNullResult();
    }
}
